#!/bin/bash

case $1 in
  ovsdb-stop ) systemctl stop ovsdb-server.service;;
  ovsdb-start ) systemctl start ovsdb-server.service;;
  ovsw-stop ) systemctl stop ovs-vswitchd.service;;
  ovsw-start ) systemctl start ovs-vswitchd.service;;
  stop ) systemctl stop ovsdb-server.service ovs-vswitchd.service;;
  status ) systemctl status ovsdb-server.service ovs-vswitchd.service|egrep "Loaded|Active";;
  vsctl-show ) ovs-vsctl show;;
  * ) echo "SET ovsdb-stop,start or ovsw-stop,start or status";;
esac
