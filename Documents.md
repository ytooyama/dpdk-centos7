# RPMパッケージを使ったOVS+DPDK環境の構築手順

本手順はrpmパッケージをリビルドしてパッケージでDPDK & OVSをインストールします。
ディストリビューションはここではCentOS 7.3を利用しています。


## 事前準備

### OSのインストールと事前確認

* CentOS 7をインストール
* いくつかのユーティリティをインストール

```
# yum install pciutils numactl psmisc
```

* NICを確認

```
# lspci |grep net

# dmesg |grep net
```

### IPアドレスを変更

ex.

```
# nmcli c m eno1 ipv4.method manual ipv4.addresses 172.16.214.111/16 ipv4.gateway 172.16.0.1 ipv4.dns 8.8.8.8 connection.autoconnect yes
```

```
# nmcli c m eno1 ipv4.method manual ipv4.addresses 172.16.214.112/16 ipv4.gateway 172.16.0.1 ipv4.dns 8.8.8.8 connection.autoconnect yes
```


### システムのアップデート

```
# yum update
```

### Hugepageの確保
GRUBにHugepageの設定を行い、確実にHugepageが確保されるように設定します。

#### Hugepageとは?

以下参照

* [RHEL7仮想化ガイド](https://access.redhat.com/documentation/ja-JP/Red_Hat_Enterprise_Linux/7/html/Virtualization_Tuning_and_Optimization_Guide/sect-Virtualization_Tuning_Optimization_Guide-Memory-Tuning.html)
* [Oracleによる解説](https://docs.oracle.com/cd/E49329_01/server.121/b71275/appi_vlm.htm)

#### GRUBで設定する方法

```
# vi /etc/default/grub

GRUB_CMDLINE_LINUX="... " 行にhugepagesの設定を行う。

default_hugepagesz=1G hugepagesz=1G hugepages=16
or
default_hugepagesz=2M hugepagesz=2M hugepages=8192

# grub2-mkconfig -o /boot/grub2/grub.cfg && reboot
(カーネルパラメーターを適用)
```

※vfioを使う場合はintel_iommu=onの指定が必要

ただし、[ハードウェアによって問題が発生する](https://access.redhat.com/solutions/1309033)ことがあるので要注意。

#### Tips: hugepagesが反映されない場合はまず確認

```
カーネルパラメータの確認
# cat /proc/cmdline

HugePageの確認
# cat /var/log/dmesg|grep huge
# cat /proc/meminfo | grep Huge
# ls /sys/devices/system/node/node0/hugepages/

vfioを使う場合はIO-MMUの確認
# find /sys | grep iommu_group
# dmesg | grep -iE "dmar|iommu"
```

うまくhugepagesが反映されない場合はCPUが大きいHugepagesをサポートしていないのかもしれない。

```
# cat /var/log/dmesg|grep huge
[    0.000000] Command line: BOOT_IMAGE=/vmlinuz-3.10.0-327.36.3.el7.x86_64 root=/dev/mapper/centos-root ro crashkernel=auto rd.lvm.lv=centos/root rd.lvm.lv=centos/swap rhgb quiet default_hugepagesz=1G hugepagesz=1G hugepages=16 iommu=pt intel_iommu=on
[    0.000000] Kernel command line: BOOT_IMAGE=/vmlinuz-3.10.0-327.36.3.el7.x86_64 root=/dev/mapper/centos-root ro crashkernel=auto rd.lvm.lv=centos/root rd.lvm.lv=centos/swap rhgb quiet default_hugepagesz=1G hugepagesz=1G hugepages=16 iommu=pt intel_iommu=on
[    0.000000] hugepagesz: Unsupported page size 1024 M  ←残念！
[    0.010872] Initializing cgroup subsys hugetlb
[    3.285619] SELinux: initialized (dev hugetlbfs, type hugetlbfs), uses transition SIDs
[    3.992595] SELinux: initialized (dev hugetlbfs, type hugetlbfs), uses transition SIDs
```

対応しているCPUはpdpe1gbフラグが出力される。されないハードウェアでは2Mの方で対応。

```
# grep pdpe1gb /proc/cpuinfo | uniq
flags           : [...] pdpe1gb [...]
```

### SELinux の無効化

DPDKとOVSの組み合わせはSELinuxに対応していないので、
公式ではpermissiveモードにしているが、どうせenforcingモードにする気が無いなら
disabledにしましょう。

```
# sed -i "s/\(^SELINUX=\).*/\1disabled/" /etc/selinux/config
```

### システムの再起動

一旦リブート

```
# reboot
```

### バージョン確認

CentOS 7.3

```
# cat /etc/centos-release
CentOS Linux release 7.3.1611 (Core)
# uname -rv
3.10.0-514.6.1.el7.x86_64 #1 SMP Wed Jan 18 13:06:36 UTC 2017
```


## DPDKとOVSのビルド(rpm) 

### 開発環境のインストール

```
# yum groupinstall "Development Tools"
# yum install gcc make epel-release git wget pciutils kernel-devel kernel-headers libpcap-devel python-six
```

### rpmbuild環境のインストール

```
# yum install rpmdevtools yum-utils
```


### DPDKパッケージのビルド

以下にspecファイルを使ったビルド方法について示す。

```
# vi /root/rpmbuild/SPECS/dpdk.spec

(下記は変更するところ)

#Version: 16.11
Version: 16.07 <-ビルドするDPDKのバージョンを指定
...
#1/13 change default:n
setconf CONFIG_RTE_LIBRTE_MBUF_OFFLOAD y
#1/11 change option
setconf CONFIG_RTE_LIBRTE_VHOST_NUMA y
#1/13 add options
setconf CONFIG_RTE_LIBRTE_VHOST y
setconf CONFIG_RTE_BUILD_COMBINE_LIBS y


# cd rpmbuild/SOURCES/
//# wget http://dpdk.org/browse/dpdk/snapshot/dpdk-16.07.tar.xz
// or
// wget http://dpdk.org/browse/dpdk/snapshot/dpdk-16.11.tar.xz

(公式からソースのダウンロード)

# LANG=C rpmbuild -v -bb /root/rpmbuild/SPECS/dpdk.spec

(DPDKのビルド開始)
```

#### DPDK Stableバージョンのビルド

Stableバージョンのビルドを同じspecファイルで実行するには、少し格好悪いけど次の方法で可能です。
例えばDPDK 16.11.1 LTSをビルドする場合は、次のように行います。

```
# wget http://dpdk.org/browse/dpdk-stable/snapshot/dpdk-stable-16.11.1.tar.xz
# tar Jxfv dpdk-stable-16.11.1.tar.xz
# mv dpdk-stable-16.11.1 dpdk-16.11.1
# tar Jcvf dpdk-16.11.1.tar.xz dpdk-16.11.1

# LANG=C rpmbuild -v -bb /root/rpmbuild/SPECS/dpdk.spec
```


### パッケージ内容を確認

```
# rpm -qilp dpdk-*.el7.centos.x86_64.rpm
# rpm -qilp dpdk-tools-*.el7.centos.x86_64.rpm
# rpm -qilp dpdk-devel-*.el7.centos.x86_64.rpm
# rpm -qilp dpdk-debuginfo-*.el7.centos.x86_64.rpm 
```

### OVSパッケージのビルド

```
# yum install openssl-devel python-twisted-core python-zope-interface desktop-file-utils groff graphviz PyQt4

# vi /root/rpmbuild/SPECS/openvswitch.spec
Version: 2.6.1
Release: 0%{?snapshot}%{?dist}-dpdk
...
%configure --enable-ssl --with-pkidir=%{_sharedstatedir}/openvswitch/pki --with-dpdk=/usr/share/dpdk/x86_64-default-linuxapp-gcc/
make %{?_smp_mflags}
(--with-dpdkパスを追記)

#%check
#%if %{with check}
#    if make check TESTSUITEFLAGS='%{_smp_mflags}' ||
#       make check TESTSUITEFLAGS='--recheck'; then :;
#    else
#        cat tests/testsuite.log
#        exit 1
#    fi
#%endif
(%checkをコメント化)

# cd /rpmbuild/SOURCES/
//# wget http://openvswitch.org/releases/openvswitch-2.6.1.tar.gz
(公式からソースのダウンロード)

# LANG=C rpmbuild -v -bb /root/rpmbuild/SPECS/openvswitch.spec
(rpmのビルド)
```

## DPDKとOVSのインストール(rpm) 

### DPDKのインストール

`yum localinstall`コマンドを使ってインストールします。指定したローカルのパッケージがインストールできます。依存するパッケージはリポジトリーからダウンロードできます。

```
# yum localinstall dpdk-*.el7.centos.x86_64.rpm  dpdk-tools-*.el7.centos.x86_64.rpm dpdk-devel-*.el7.centos.x86_64.rpm
```


### DPDKの動作確認

```
# dpdk-devbind --status

Network devices using DPDK-compatible driver
============================================
<none>

Network devices using kernel driver
===================================
0000:01:00.0 'NetXtreme II BCM5709 Gigabit Ethernet' if=em1 drv=bnx2 unused= *Active*
0000:01:00.1 'NetXtreme II BCM5709 Gigabit Ethernet' if=em2 drv=bnx2 unused=
0000:02:00.0 'NetXtreme II BCM5709 Gigabit Ethernet' if=em3 drv=bnx2 unused=
0000:02:00.1 'NetXtreme II BCM5709 Gigabit Ethernet' if=em4 drv=bnx2 unused=
0000:07:00.0 '82575GB Gigabit Network Connection' if=p2p1 drv=igb unused=
0000:07:00.1 '82575GB Gigabit Network Connection' if=p2p2 drv=igb unused=
0000:08:00.0 '82575GB Gigabit Network Connection' if=p2p3 drv=igb unused=
0000:08:00.1 '82575GB Gigabit Network Connection' if=p2p4 drv=igb unused=

Other network devices
=====================
<none>
```


### OVSのインストール

```
# yum localinstall openvswitch-*.el7.centos.x86_64.rpm openvswitch-devel-*.el7.centos.x86_64.rpm openvswitch-test-*.el7.centos.noarch.rpm python-openvswitch-*.el7.centos.noarch.rpm
```

### OVSの永続化

```
# modprobe openvswitch  #一時的
# echo "openvswitch" > /etc/modules-load.d/openvswitch.conf  #永続化
```

### NICのバインド

ドライバーについては[Binding NIC drivers](http://dpdk-guide.gitlab.io/dpdk-guide/setup/binding.html)を参考に。

#### uio_pci_generic

一番一般的に使えるドライバー。カーネルのメインラインに入っている。
一部のNICでは使えないのと対応していない機能がある点に注意

```
# modprobe uio_pci_generic  #一時的
# echo "uio_pci_generic" > /etc/modules-load.d/uio_pci_generic.conf  #永続化
```

#### igb_uio

uio_pci_genericよりもパフォーマンスが良いドライバー。
DPDKのビルド時に一緒にビルドしたものを使う。

```
# depmod -a
# modprobe uio
# modprobe igb_uio

# echo "uio" >> /etc/modules-load.d/igb_uio.conf  #永続化
# echo " igb_uio" >> /etc/modules-load.d/igb_uio.conf  #永続化
```

#### VFIO

よりパフォーマンスが高いが、環境を選ぶドライバー。
カーネルのメインラインに入っている。VFIO+IOMMUの利用はハードウェアによってハマるかもしれない。。


##### VFIO

vt-dが必要。BIOSでの設定有効、カーネルパラメータでのintel_iommu=onが必要。

```
# modprobe vfio-pci
# echo 1 > /sys/module/vfio_iommu_type1/parameters/allow_unsafe_interrupts

# echo "vfio-pci" > /etc/modules-load.d/vfio-pci.conf                        #永続化
# echo "options vfio_iommu_type1 allow_unsafe_interrupts=1" > /etc/modules-load.d/iommu.conf  #永続化
```

##### VFIO-noIOMMU

vt-dがオフの場合はこちらのモードで実行可能。本来はKernel 4.5以降で使えるモードだが、RHEL/CentOS 7だとバックポートされている。

```
# modprobe vfio-pci
# echo 1 > /sys/module/vfio/parameters/enable_unsafe_noiommu_mode

# echo "vfio-pci" > /etc/modules-load.d/vfio-pci.conf  #永続化
# echo "options vfio enable_unsafe_noiommu_mode=1" > /etc/modules-load.d/vfio-noiommu.conf  #永続化
```



### モジュールなどの確認

```
# cat /var/log/dmesg|grep huge
# lsmod | egrep "uio_pci_generic|igb_uio|vfio_pci" && lsmod | grep openvswitch
# cat /proc/meminfo | grep Huge
```

### モジュールをデバイスに割り当て

* uio_pci_genericで良いなら

```
# dpdk-devbind --status
# dpdk-devbind -b uio_pci_generic 0000:04:00.0
```

* igb_uioを使うなら

```
# dpdk-devbind --status
# dpdk-devbind -b igb_uio 0000:04:00.0
```

* vfio-pciを使うなら

```
# dpdk-devbind -b vfio-pci 0000:04:00.0
```

※kernel 3.6以上でVT-d (IOMMU)が使える環境だと、よりセキュアなvfioも使えます。

* システムで使えるか確認

```
# dmesg | egrep "DMAR|IOMMU" 
```

### NICバインドの確認

```
# dpdk-devbind --status
Network devices using DPDK-compatible driver
============================================
0000:05:00.0 '82571EB Gigabit Ethernet Controller' drv=uio_pci_generic unused=e1000e
...
```


## Start OVS

### クリーンアップ

```
# systemctl stop ovs-vswitchd.service ovsdb-server.service
# systemctl status ovs-vswitchd.service ovsdb-server.service
# rm /etc/openvswitch/conf.db ; rm /etc/openvswitch/.conf.db.~lock~
```

### Hugepageの確認

```
# cat /proc/meminfo | grep Huge
```

### OVSの設定

* OVSに設定できるパラメーターはマニュアルを参照
  * http://manpages.ubuntu.com/manpages/zesty/man5/ovs-vswitchd.conf.db.5.html
  * http://openvswitch.org/support/dist-docs/ovs-vswitchd.conf.db.5.txt

* 別の端末を開いてモニタリング

```
# tailf /var/log/openvswitch/ovs-vswitchd.log
```

* OVSの設定を行う

```
# ovsdb-tool create /etc/openvswitch/conf.db /usr/share/openvswitch/vswitch.ovsschema

# systemctl start ovsdb-server.service

# ovs-vsctl --no-wait set Open_vSwitch . other_config:dpdk-init=true  #DPDKを有効化

# ovs-vsctl --no-wait set Open_vSwitch . other_config:vhost-sock-dir=/

# ovs-vsctl --no-wait set Open_vSwitch . other_config:pmd-cpu-mask=0xF
# ovs-vsctl --no-wait set Open_vSwitch . other_config:dpdk-lcore-mask=0x3F0
# ovs-vsctl --no-wait set Open_vSwitch . other_config:dpdk-socket-mem=4096

# ovs-vsctl --no-wait get Open_vSwitch . other_config   #設定の確認

# systemctl start ovs-vswitchd.service
# systemctl status ovsdb-server.service ovs-vswitchd.service|egrep "Loaded|Active"

# ovs-vsctl show
```


* 割り当ての考え方

```
4コア x 2ソケット x 2スレッド（Hyper thread）のマシンがあるとすると
16論理コアに見える訳ですが、
それを、2進数で表します。
0000 0000 0000 0000 = 0x0 何処にも割り当ててない場合
0000 0000 0000 0001 = 0x1 CPU番号0番に割り当てた場合
0000 0000 0000 0010 = 0x2 CPU番号1番に割り当てた場合
0000 0000 0000 0011 = 0x3 CPU番号0と1に割り当てた場合

こんな感じで計算します。  

15 14 13 12    11 10 9 8    7 6 5 4   3 2 1 0 の順番になってます。
現実には、これにNUMAやNICがどのCPUのPCIeレーンに収容されているかなどを
判断する必要があります。
```

ex.

```
# numactl --hardware
```

2pの場合
15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0	CPU
0	1	0	1	0	1	0	1	0	1	0	1	0	1	0	1	numa0
1	0	1	0	1	0	1	0	1	0	1	0	1	0	1	0	numa1

1pの場合
15	14	13	12	11	10	9	8	7	6	5	4	3	2	1	0	CPU
1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	numa0
						1	1	1	1	1	1	0	0	0	0 

2->16進数に変換する

CPU 0-3 -> 1111 -> 0xF
CPU 4-9 -> 1111110000 -> 0x3F0


### OVSポートの作成

```
# ovs-vsctl add-br ovsbr0 -- set bridge ovsbr0 datapath_type=netdev
# ovs-vsctl add-port ovsbr0 vhost-user1 -- set Interface vhost-user1 type=dpdkvhostuser
# ovs-vsctl add-port ovsbr0 vhost-user2 -- set Interface vhost-user2 type=dpdkvhostuser

# ovs-vsctl add-port ovsbr0 dpdk0 -- set Interface dpdk0 type=dpdk
```

最後のコマンドがエラーとなるときは、まず`/var/log/openvswitch/ovs-vswitchd.log`を確認。dpdk-devbindでDPDKドライバーをデバイスにバインドしましたか?


#### Tips: OVSのポートを消す手順

```
# ovs-vsctl del-port ovsbr0 vhost-user2
# ovs-vsctl del-port ovsbr0 vhost-user1
# ovs-vsctl del-port ovsbr0 dpdk0
# ovs-vsctl del-br ovsbr0
```

#### DPDKドライバーの切り替え

```
# ovs-vsctl del-port ovsbr0 dpdk0
# dpdk-devbind -u 0000:04:00.0           #アンバインド

# dpdk-devbind --status
# dpdk-devbind -b vfio-pci 0000:04:00.0  #新しいモジュールをデバイスに割り当て

# systemctl restart ovsdb-server.service
# systemctl restart ovs-vswitchd.service

# ovs-vsctl add-port ovsbr0 dpdk0 -- set Interface dpdk0 type=dpdk
```

#### 確認するためのコマンド

NumaとDPDKのステータスを確認

```
# ovs-appctl dpif-netdev/pmd-stats-show
```

Numaの確認

```
# numactl --hardware
```

* [参考資料](https://access.redhat.com/documentation/ja/red-hat-openstack-platform/8/single/configure-dpdk-for-openstack-networking/#key_considerations)


## Linux KVMでのOVS-DPDKデバイスの利用

### Qemu & Linux KVMのインストール

```
# yum install centos-release-qemu-ev
# yum install qemu-kvm-ev libvirt virt-install 
```

### QEMUの設定を変更

```
# vi /etc/libvirt/qemu.conf

user = "root"  #コメントを外す
group = "root" #コメントを外す

# systemctl enable libvirtd 
# systemctl start libvirtd
```

### Virt-Managerなどで以下のようなVMを作成、OSのインストール

(1) VMを作成

1個目のNICはNATを使うと良いかも

(2) VMにOSをインストール(ex. CentOS 7.3)

### DPDKデバイスをNICとして追加

VMに対して別々のvhost-userを割り当てます。
次のコマンドでデバイスやそのほかのパラメーターを追加します。

(3) `virsh edit vmname`コマンドで以下の設定を追加

共通

```
<currentMemory unit='KiB'>1048576</currentMemory>
<memoryBacking>
    <hugepages/>
  </memoryBacking>
(memoryBackingを追加)

  <cpu mode='host-passthrough'>
    <numa>
      <cell id='0' cpus='0' memory='1048576' unit='KiB' memAccess='shared'/>
    </numa>
 </cpu>
(numaを追加.numaのmemoryサイズはcurrentMemory unitと同じサイズにする)
```

```
# virsh edit vm1

    <interface type='vhostuser'>
      <source type='unix' path='/var/run/openvswitch/vhost-user1' mode='client'/>
      <model type='virtio'/>
    </interface>

# virsh edit vm2

    <interface type='vhostuser'>
      <source type='unix' path='/var/run/openvswitch/vhost-user2' mode='client'/>
      <model type='virtio'/>
    </interface>
```

(4) `virsh edit vmname`コマンドで以下の設定を追加

* [5.5.2. マルチキュー virtio-net](https://access.redhat.com/documentation/ja-JP/Red_Hat_Enterprise_Linux/7/html/Virtualization_Tuning_and_Optimization_Guide/sect-Virtualization_Tuning_Optimization_Guide-Networking-Techniques.html#sect-Virtualization_Tuning_Optimization_Guide-Networking-Multi-queue_virtio-net) の設定を、「interface type='vhostuser'」に対して行います。

(5) VMを起動する

`virsh start`コマンドでVMを起動し、エラーが出ないことを確認します。
起動するとNICが正常であればDHCPでIPアドレスを勝手に取得して繋がります。

```
# virsh start vmname

VMが起動したら実行(Mは手順4で指定した1-Nまでの範囲の値を指定)
# ethtool -L eth1 combined M
```

# ベンチマークテスト

## iperf3編

### パッケージ導入

```
# yum install iperf3
# firewall-cmd --permanent --zone=public --add-port=5001/udp && firewall-cmd --reload
# firewall-cmd --permanent --zone=public --add-port=5001/tcp && firewall-cmd --reload
```

### クライアント

```
# iperf3 -t 15 -i 1 -c <サーバーアドレス>
```

### サーバー

IPアドレスの確認

```
# ip -f inet -o a s eth0|awk '{print $2,$4}'
# ip -f inet -o a s eth1|awk '{print $2,$4}'
```

サーバー起動

```
# iperf3 -s -B 0.0.0.0
```
